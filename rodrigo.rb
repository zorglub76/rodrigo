﻿require 'sinatra'
require 'omniauth'

use Rack::Session::Cookie # OmniAuth requires sessions.
use OmniAuth::Builder do
	provider :openid, nil, :name => 'google', :identifier => 'https://www.google.com/accounts/o8/id'
end

get '/' do
	if (session[:auth] == nil)
		@registered = false
	else
		@registered = true
		@name = session[:auth]['user_info']['name']
		@email = session[:auth]['user_info']['email']
	end
	erb :index
end

get '/test_case' do
	if (session[:auth] == nil)
		@registered = false
	else
		@registered = true
		@name = session[:auth]['user_info']['name']
		@email = session[:auth]['user_info']['email']
	end
	erb :test_case
end

post '/auth/:provider/callback' do  
	session[:auth] = request.env['omniauth.auth']
	redirect "/"
	#"{\"name\":\"#{auth['user_info']['name']}\",\"provider\":\"#{params['provider']}\"}"
end

get '/logout' do
	session[:auth] = nil
	redirect '/'
end