(function () {
	"use strict";
	$(function () {
		var config = {
			toolbar: [
				['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
				['UIColor']
			]
		};

		$('.editor').ckeditor(config);
	});
}());